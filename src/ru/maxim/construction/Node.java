package ru.maxim.construction;

public class Node 
{
    private double				    x;
    private int					    stress;
    private boolean				    isSealing;
    
    
    
    public				    Node()
    {
	x = 0;
	isSealing = false;
    }
    
    public				    Node(double x)
    {
	this.x = x;
    }
    
    public double			    getX()
    {
	return x;
    }
    
    public void				    setX(double x)
    {
	this.x = x;
    }
    
    public void				    setStress(int stress)
    {
	this.stress = stress;
    }
    
    public int				    getStress()
    {
	return stress;
    }
    
    public void				    setSealing(boolean isSealing)
    {
	this.isSealing = isSealing;
    }
    
    public boolean			    isSealing()
    {
	return isSealing;
    }

}
