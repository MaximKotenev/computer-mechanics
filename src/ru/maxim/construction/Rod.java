package ru.maxim.construction;

public class Rod
{
    private double				    length;
    private int					    square;
    private double				    x;
    private double				    height;
    
    private int					    elasticModulus;
    private int					    limitingStress;    
    
    private int					    stress;



    public				    Rod(int length, int square, int elasticModulus, int limitingStress) 
    {
	this.length = length;
	this.square = square;
	this.elasticModulus = elasticModulus;
	this.limitingStress = limitingStress;
	
	height = (double) square / (double) length;
    }
    
    private void			    calculateHeight()
    {
	height = (double) square / (double) length;
    }
    
    public void				    setStress(int stress)
    {
	this.stress = stress;
    }
    
    public int				    getStress()
    {
	return stress;
    }
    

    public double			    getX() 
    {
	calculateHeight();
	return x;
    }

    public void				    setX(double x) 
    {
	calculateHeight();
	this.x = x;
    }

    public double			    getLength() 
    {
	calculateHeight();
	return length;
    }

    public void				    setLength(double length) 
    {
	calculateHeight();
	this.length = length;
    }

    public int				    getSquare() 
    {
	calculateHeight();
	return square;
    }

    public void				    setSquare(int square) 
    {
	calculateHeight();
	this.square = square;
    }

    public int				    getElasticModulus() 
    {
	return elasticModulus;
    }

    public void				    setElasticModulus(int elasticModulus) 
    {
	this.elasticModulus = elasticModulus;
    }

    public int				    getLimitingStress() 
    {
	return limitingStress;
    }

    public void				    setLimitingStress(int limitingStress) 
    {
	this.limitingStress = limitingStress;
    }

    public double			    getHeight() 
    {
	calculateHeight();
	return height;
    }

    public void				    setHeight(double height) 
    {
	calculateHeight();
	this.height = height;
    }
}
