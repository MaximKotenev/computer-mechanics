package ru.maxim.construction;

import java.util.ArrayList;
import java.util.List;

public class SchemeContainer
{
    private static List<Rod>			    rods = new ArrayList<>();
    private static List<Node>			    nodes = new ArrayList<>();
    
    
    
    public static void			    addRod(Rod rod)
    {
	rods.add(rod);
	recalculate();
    }
    
    public static void			    removeRod(int index)
    {
	double length = rods.get(index).getLength();
	if (index >= 0 && index < rods.size())
	    rods.remove(index);
	for (Rod r: rods)
	    r.setX(r.getX() - length);
	recalculate();
    }
    
    public static void			    recalculate()
    {
	recalculateRods();
	recalculateNodes();
    }
    
    
    private static void			    recalculateRods()
    {
        if (rods.isEmpty())
            return;
        
	rods.get(0).setX(0);
	for (int i = 1; i < rods.size(); i++)
	{
	    double x1 = (rods.get(i - 1).getX()) + (rods.get(i - 1).getLength());
	    rods.get(i).setX(x1);
	}
    }
    
    private static void			    recalculateNodes()
    {
	nodes.clear();
	for (int i = 0; i < rods.size(); i++)
	{
	    double x1 = rods.get(i).getX();
	    if (findSameNode(x1))
		nodes.add(new Node(x1));

	    double x2 = rods.get(i).getX() + rods.get(i).getLength();
	    if (findSameNode(x2))
		nodes.add(new Node(x2));
	}
    }
    
    private static boolean		    findSameNode(double x)
    {
	for (Node n: nodes)
	    if (n.getX() == x)
		return false;
	return true;
    }

    
    public static List<Rod>		    getRods() 
    {
	return rods;
    }
    
    public static List<Node>		    getNodes() 
    {
	return nodes;
    }
    
    
    public static int			    getMaxHeight()
    {
	return rods.stream().map(rod -> rod.getHeight()).reduce((s1,s2) -> s1 > s2 ? s1: s2).get().intValue();
    }
    
    public static int			    getMaxSquare()
    {
	return rods.stream().map(rod -> rod.getSquare()).reduce((s1,s2) -> s1 > s2 ? s1: s2).get();
    }
    
    public static double		    getWholeLength()
    {
	double res = 0;
	res = rods.stream().map(rod -> rod.getLength()).reduce(res, Double::sum);
	return res;
    }
    
    public static boolean		    checkSealingBySides()
    {
	if (nodes.get(0).isSealing() || nodes.get(nodes.size()-1).isSealing())
	    return true;
	return false;
    }
    
    public static void			    printRods()
    {
	for (Rod rod: rods)
	    System.out.println("x: " + rod.getX() + ", L: " + rod.getLength());
    }

    public static void                      setRods(List<Rod> rods)
    {
        SchemeContainer.rods = rods;
    }
    
    public static void                      setNodes(List<Node> nodes)
    {
        SchemeContainer.nodes = nodes;
    }
}