package ru.maxim.postprocessor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.processor.MainCalculations;

public class NxTableFrame extends javax.swing.JFrame 
{
    private javax.swing.JScrollPane	    jScrollPane1;
    private javax.swing.JTable		    tableNx;
    private MyTableModel		    model;

    private List<Rod>			    rods;
    private MainCalculations		    calculations;
    private List<Bean>			    beans;
    
    
    
    public			    NxTableFrame() 
    {
	reInit(5);
        initComponents();
    }
    
    public void			    reInit(int discret)
    {
	rods = SchemeContainer.getRods();
	calculations = MainCalculations.getInstance();
	
	if (beans == null) beans = new ArrayList<>();
	beans.clear();
	for (int i = 0; i < rods.size(); i++)
		for (double x = 0, j = 0; x <= rods.get(i).getLength(); x += rods.get(i).getLength() / discret, j++)
		    beans.add(new Bean(
                            rods.get(i),
                            x,
                            calculations.calculateNx(i, x),
                            calculations.calculateUx(i, x),
                            i,
                            (int) j));

	if (model == null)
	    model = new MyTableModel(beans);
	model.reInit(beans);
	
	if (tableNx != null)
	    tableNx.updateUI();
    }
    
    public class Bean
    {
	public Rod rod;
	public double x;
	public double nx;
	public double ux;
	public double sigma;
	public int numberRod;
	public int numberIn;
	
	public Bean(Rod rod, double x, double nx, double ux, int numberRod, int numberIn) {
	    this.rod = rod;
	    this.x = x;
	    this.nx = nx;
	    this.ux = ux;
	    this.sigma = nx / rod.getSquare();
	    this.numberRod = numberRod;
	    this.numberIn = numberIn;
	}
    }
    
    public class MyTableModel implements TableModel 
    {
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>(); 
        private List<Bean> beans;
 
        public MyTableModel(List<Bean> beans) {
            this.reInit(beans);
        }
	
	public void reInit(List<Bean> beans)
	{
	    this.beans = beans;
	}
 
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
        public Class<?> getColumnClass(int columnIndex) {
	    switch (columnIndex) 
	    {
		case 0:
		    return Integer.class;
		case 1:
		    return Integer.class;
		case 2:
		    return Double.class;
		case 3:
		    return Double.class;
		case 4:
		    return Double.class;
		case 5:
		    return Double.class;
		case 6:
		    return Integer.class;
		case 7:
		    return Boolean.class;
	    }
	    return Object.class;
        }
 
	@Override
        public int getColumnCount() {
            return 8;
        }
 
	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) 
	    {          
		case 0:
		    return "Номер стержня";
		case 1:
		    return "№";
		case 2:
		    return "x";
		case 3:
		    return "Nx";
		case 4:
		    return "u";
		case 5:
		    return "Sigma";
		case 6:
		    return "Напряжение";
		case 7:
		    return "!";
            }
            return "";
        }
 
        public int getRowCount() {
            return beans.size();
        }
 
        public Object getValueAt(int rowIndex, int columnIndex) {
            Bean bean = beans.get(rowIndex);
            switch (columnIndex) 
	    {
		case 0:
		    return bean.numberRod;
		case 1:
		    return bean.numberIn;
		case 2:
		    return bean.x;
		case 3:
		    return bean.nx;
		case 4:
		    return bean.ux;
		case 5:
		    return bean.sigma;
		case 6:
		    return bean.rod.getLimitingStress();
		case 7:
		    return Math.abs(bean.sigma) > Math.abs(bean.rod.getLimitingStress());
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return true;
        }
 
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
        public void setValueAt(Object value, int rowIndex, int columnIndex) 
	{
        }
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableNx = new javax.swing.JTable(model);

        jScrollPane1.setViewportView(tableNx);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>
    
    public static void		    main(String[] args) {
	
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30

	
	new NxTableFrame().setVisible(true);
    }
}
