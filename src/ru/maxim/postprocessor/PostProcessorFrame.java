package ru.maxim.postprocessor;

import javax.swing.JOptionPane;

public class PostProcessorFrame extends javax.swing.JFrame 
{
    private AbstractDraw		    drawEpure;
    private AbstractDraw		    drawSigma;
    private DrawU       		    drawU;
    private NxTableFrame		    tableNx;
    private ByCoordinateFrame		    byCoordinateFrame;
    
    private javax.swing.JButton	    	    buttonDrawEpure;
    private javax.swing.JButton	    	    buttonDrawSigma;
    private javax.swing.JButton		    buttonTableFull;
    private javax.swing.JButton		    buttonDrawU;
    private javax.swing.JButton		    buttonByCoordinate;



    public PostProcessorFrame() 
    {
	initComponents();

	drawEpure = new DrawEpure();
	drawSigma = new DrawSigma();	
	drawU = new DrawU();
	tableNx = new NxTableFrame();
	byCoordinateFrame = new ByCoordinateFrame();
	
	buttonDrawEpure.addActionListener((e) -> {
	    drawEpure.setVisible(true);
	});
	
	buttonDrawSigma.addActionListener((e) -> {
	    drawSigma.setVisible(true);
	});
	
	buttonDrawU.addActionListener((e) -> {
	    drawU.setVisible(true);
	});
	
	buttonTableFull.addActionListener((e) -> {
             String strDiscret = JOptionPane.showInputDialog("Установите дискретизацию расчета");
             if (strDiscret == null)
                return;
            if (strDiscret.isEmpty())
                return;
            
	    int discret = Integer.parseInt(strDiscret);
	    tableNx.reInit(discret);
	    tableNx.setVisible(true);
	});

	buttonByCoordinate.addActionListener((e) -> {
            String strNumRod = JOptionPane.showInputDialog("Номер стержня");
            String strX = JOptionPane.showInputDialog("x");
            if (strNumRod == null || strX == null)
                return;
            if (strNumRod.isEmpty() || strX.isEmpty())
                return;
            
	    int numRod = Integer.parseInt(strNumRod) - 1;
	    double x = Double.parseDouble(strX);
            boolean reInit = byCoordinateFrame.reInit(numRod, x);
	    byCoordinateFrame.setVisible(reInit);
	});
    }

    // <editor-fold defaultstate="collapsed" desc="initComponents()">//GEN-BEGIN:initComponents
    private void initComponents() 
    {
        buttonDrawEpure = new javax.swing.JButton();
        buttonDrawSigma = new javax.swing.JButton();
        buttonDrawU = new javax.swing.JButton();
        buttonTableFull = new javax.swing.JButton();
        buttonByCoordinate = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        buttonDrawEpure.setText("Отрисовать N(x)");
        buttonDrawSigma.setText("Отрисовать Sigma(x)");
        buttonDrawU.setText("Отрисовать u(x)");
        buttonTableFull.setText("Полная таблица");
        buttonByCoordinate.setText("По координате");

	javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonByCoordinate, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(buttonTableFull, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonDrawSigma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonDrawU, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonDrawEpure, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)))
                .addContainerGap(163, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(buttonDrawEpure, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonDrawSigma, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonDrawU, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonTableFull, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(buttonByCoordinate, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:Old shit

    public static void main(String args[]) 
    {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PostProcessorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PostProcessorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PostProcessorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PostProcessorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PostProcessorFrame().setVisible(true);
            }
        });
    }


}
