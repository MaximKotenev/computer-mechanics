package ru.maxim.postprocessor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.processor.MainCalculations;

public abstract class AbstractDraw extends JPanel
{
    public static int				    WIDTH = 640;
    public static int				    HEIGHT = 480;

    private BufferedImage			    image;
    private Graphics				    g;
    private Graphics2D				    g2;
    protected Color                                 color;
    private JFrame                                  frame;

    private double				    xCoof;
    private double				    yCoofPositive;
    private double				    yCoofMinus;
    
    protected MainCalculations			    calc;
    protected List<Rod>				    rods;
    protected double[]				    arrayValues;



    public				    AbstractDraw()
    {
	setPreferredSize(new Dimension(WIDTH, HEIGHT));

	image = new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_RGB);
	g = (Graphics2D) image.getGraphics();
        
        frame = new JFrame();
        frame.setContentPane(this);
        frame.pack();
    }

    public void                             setVisible(boolean b)
    {
        frame.setVisible(b);
    }
    
    protected abstract void                   initValues();

    private boolean			    init(Graphics g)
    {
	calc = MainCalculations.getInstance();
	rods = SchemeContainer.getRods();
        
	initValues();
	if (rods.isEmpty() || arrayValues.length == 0) 
	    return false;

	xCoof = WIDTH / SchemeContainer.getWholeLength();
	
	yCoofPositive = HEIGHT/2 / Arrays.stream(arrayValues).max().getAsDouble();
	yCoofMinus = Math.abs(HEIGHT/2 / Arrays.stream(arrayValues).min().getAsDouble());
	
	g2 = (Graphics2D) g;
	return true;
    }

    @Override
    public void				    paintComponent(Graphics g)
    {	
	if (!init(g))
	    return;
	
	clearScreen();
	drawEpures();
    }

    private void			    clearScreen()
    {
	g2.setColor(Color.WHITE);
	g2.fillRect(0, 0, WIDTH, HEIGHT);
    }

    private void			    drawEpures()
    {
	g2.setColor(Color.BLACK);
	g2.drawLine(0, HEIGHT/2, WIDTH, HEIGHT/2);
	for (Rod rod : rods)
	{	 
	    g2.drawLine((int) (rod.getX() * xCoof), 0, (int) (rod.getX() * xCoof), HEIGHT);
	    g2.setColor(Color.BLACK);
	}
	
	g2.setColor(color);
	for (int i = 0, j = 0; i < rods.size(); i++, j += 2)
	{
	    Rod rod = rods.get(i);
            drawEpure(rod, arrayValues[j], arrayValues[j + 1]);
	}
    }

    private void			    drawEpure(Rod rod, double y1, double y2)
    {
	double y1T = y1;
	double y2T = y2;
	
	y1 = translateCoordinates(y1);
	y2 = translateCoordinates(y2);
	
	g2.drawLine((int) (rod.getX() * xCoof), (int) y1, 
		(int) (rod.getX() * xCoof + rod.getLength()* xCoof), (int) y2);
	g2.drawString("" + String.format("%.2f", y1T),
		(int) (rod.getX() * xCoof), (int)y1 + 5);
	g2.drawString("" + String.format("%.2f", y2T),
		(int) (rod.getX() * xCoof + rod.getLength()* xCoof), (int)y2 + 5);
    }
    
    private double			    translateCoordinates(double y)
    {
	if (y > 0)
	{
	    y *= yCoofPositive;
	    y = HEIGHT/2 - y;
            y += 10;                // отодвинуть от границ сверху
	}
	else
	{
	    y = Math.abs(y);
	    y *= yCoofMinus;
	    y += HEIGHT/2;
            y -= 10;                // отодвинуть от границ снизу
	}
	
	return y;
    }
    
    @Override
    public Dimension			    getPreferredSize()
    {
	return new Dimension(WIDTH, HEIGHT);
    }
}
