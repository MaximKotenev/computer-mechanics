package ru.maxim.postprocessor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.processor.MainCalculations;

public class DrawU extends JPanel
{
    public static int				    WIDTH = 640;
    public static int				    HEIGHT = 480;

    private BufferedImage			    image;
    private Graphics				    g;
    private Graphics2D				    g2;
    
    private double				    xCoof;
    private double				    yCoofPositive;
    private double				    yCoofMinus;
    
    private MainCalculations			    calc;
    private List<Rod>				    rods;
    private List<Double>			    ux;
    private int                                     discret;
    
    private final JFrame                            frame;



    public				    DrawU()
    {
	setPreferredSize(new Dimension(WIDTH, HEIGHT));

	image = new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_RGB);
	g = (Graphics2D) image.getGraphics();
        
        frame = new JFrame();
        frame.setContentPane(this);
        frame.pack();
    }
    
    @Override
    public void                             setVisible(boolean b)
    {
        frame.setVisible(b);
    }
        
    private boolean			    init(Graphics g)
    {
        discret = 10;
	calc = MainCalculations.getInstance();
	rods = SchemeContainer.getRods();
	ux = new ArrayList();
	if (rods.isEmpty())
	    return false;
	
	for (int i = 0; i < rods.size(); i++)
            for (double j = 0; j < rods.get(i).getLength(); j += rods.get(i).getLength() / discret)
                ux.add(calc.calculateUx(i, j));


	xCoof = WIDTH / SchemeContainer.getWholeLength();
	yCoofPositive = HEIGHT/2 / ux.stream().max(Double::compareTo).get();
	yCoofMinus = Math.abs(HEIGHT/2 / ux.stream().min(Double::compareTo).get());
        
	g2 = (Graphics2D) g;
	return true;
    }
    
    @Override
    public void				    paintComponent(Graphics g)
    {	
	if (!init(g))
	    return;
	
	clearScreen();
	drawEpures();
        
    }

    private void			    clearScreen()
    {
	g2.setColor(Color.WHITE);
	g2.fillRect(0, 0, WIDTH, HEIGHT);
    }
    
    private void			    drawEpures()
    {
	g2.setColor(Color.BLACK);
	g2.drawLine(0, HEIGHT/2, WIDTH, HEIGHT/2);
	for (Rod rod : rods)
	{	 
	    g2.drawLine((int) (rod.getX() * xCoof), 0, (int) (rod.getX() * xCoof), HEIGHT);
	    g2.setColor(Color.BLACK);
	}
	
	g2.setColor(Color.GRAY);
	for (int r = 0; r < rods.size(); r++)
	{
	    Rod rod = rods.get(r);
	    drawEpure(rod, r);
	}
    }
    
    private void			    drawEpure(Rod rod, int i)
    {	
        double step = rod.getLength() / discret;

        for (double x = rod.getX(), p = 0;                                      // p - для расчета локальных координат
                x < rod.getX() + rod.getLength();// && p + step < rod.getLength();
                x += step, p += step)                                           // x - для глобальных
        {
            double y1 = translateCoordinates(calc.calculateUx(i, p));
            double y2 = translateCoordinates(calc.calculateUx(i, p + step));
            
            if (p+step < rod.getLength())
                g2.drawLine(
                        (int) (x * xCoof),
                        (int) y1,
                        (int) ((x + step) * xCoof),
                        (int) y2
                );
            
//            System.out.println("x: " + x + "; y1: " + calc.calculateUx(i, p) + 
//                    "; y2: " + calc.calculateUx(i, p + step));
        }

	g2.drawString("" + String.format("%.2f", calc.calculateUx(i, 0)),
		(int) (rod.getX() * xCoof), 
                (int) translateCoordinates(calc.calculateUx(i, 0)) + 5);
	g2.drawString("" + String.format("%.2f", calc.calculateUx(i, rod.getLength())),
		(int) (rod.getX() * xCoof + rod.getLength()* xCoof),
                (int)translateCoordinates(calc.calculateUx(i, rod.getLength())) + 5);
        
//        System.out.println("------------------------------");
    }
    
    private double			    translateCoordinates(double y)
    {
	if (y > 0)
	{
	    y *= yCoofPositive;
	    y = HEIGHT/2 - y;
//            y += 10;                // отодвинуть от границ сверху
	}
        else if (y < 0)
	{
	    y = Math.abs(y);
	    y *= yCoofMinus;
	    y += HEIGHT/2;
//            y -= 10;                // отодвинуть от границ снизу
	}
        else
            y = HEIGHT / 2;
	
	return y;
    }
    
    @Override
    public Dimension			    getPreferredSize()
    {
	return new Dimension(WIDTH, HEIGHT);
    }
}
