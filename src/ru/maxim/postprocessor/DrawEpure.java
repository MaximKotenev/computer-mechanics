package ru.maxim.postprocessor;

import java.awt.Color;

public class DrawEpure extends AbstractDraw
{
    @Override
    protected void initValues() 
    {
        arrayValues = new double[rods.size() * 2];
        for (int i = 0, j = 0; i < rods.size(); i++, j += 2) {
            arrayValues[j] = calc.calculateNx(i, 0);
            arrayValues[j+1] = calc.calculateNx(i, rods.get(i).getLength());
        }
        
        color = Color.RED;
    }
    
}