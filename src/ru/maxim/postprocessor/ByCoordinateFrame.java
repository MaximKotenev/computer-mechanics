package ru.maxim.postprocessor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.processor.MainCalculations;

public class ByCoordinateFrame extends JFrame
{
    private javax.swing.JScrollPane	    jScrollPane1;
    private javax.swing.JTable		    table;
    private MyTableModel		    model;

    private List<Rod>			    rods;
    private MainCalculations		    calculations;
    private Bean    			    bean;



    public			    ByCoordinateFrame() 
    {
        initValues();
        initComponents();
    }
    
    private void                    initValues()
    {
	if (model == null)
	    model = new MyTableModel(bean);
	model.reInit(bean);
	
	if (table != null)
	    table.updateUI();
    }
    
    public boolean                  reInit(int numberRod, double x)
    {
	rods = SchemeContainer.getRods();
	calculations = MainCalculations.getInstance();
        if (numberRod >= rods.size() || numberRod < 0 || x < 0)
            return false;
        if (x > rods.get(numberRod).getLength())
            return false;

	bean = new Bean(rods.get(numberRod), x, calculations.calculateNx(numberRod, x),
			calculations.calculateUx(numberRod, x), numberRod);
        initValues();
        
        return true;
    }
    
    public class Bean
    {
	public Rod rod;
	public double x;
	public double nx;
	public double ux;
	public double sigma;
	public int numberRod;
	
	public Bean(Rod rod, double x, double nx, double ux, int numberRod) {
	    this.rod = rod;
	    this.x = x;
	    this.nx = nx;
	    this.ux = ux;
	    sigma = nx / rod.getSquare();
	    this.numberRod = numberRod;
	}
    }
    
    public class MyTableModel implements TableModel 
    {
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>(); 
        private Bean bean;
 
        public MyTableModel(Bean bean) {
            this.reInit(bean);
        }
	
	public void reInit(Bean bean)
	{
	    this.bean = bean;
	}
 
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
        public Class<?> getColumnClass(int columnIndex) {
	    switch (columnIndex) 
	    {
		case 0:
		    return Integer.class;
		case 1:
		    return Double.class;
		case 2:
		    return Double.class;
		case 3:
		    return Double.class;
		case 4:
		    return Double.class;
		case 5:
		    return Integer.class;
	    }
	    return Object.class;
        }
 
	@Override
        public int getColumnCount() {
            return 5;
        }
 
	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) 
	    {          
		case 0:
		    return "Номер стержня";
		case 1:
		    return "x";
		case 2:
		    return "Nx";
		case 3:
		    return "Sigma";
		case 4:
		    return "u";
		case 5:
		    return "Напряжение";
            }
            return "";
        }
 
        public int getRowCount() {
            return 1;
        }
 
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) 
	    {
		case 0:
		    return bean.numberRod;
		case 1:
		    return bean.x;
		case 2:
		    return bean.nx;
		case 3:
		    return bean.sigma;
		case 4:
		    return bean.ux;
		case 5:
		    return bean.rod.getLimitingStress();
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return true;
        }
 
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
        public void setValueAt(Object value, int rowIndex, int columnIndex) 
	{
        }
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(model);

        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>
    
    public static void		    main(String[] args) {
	
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30

	
	new NxTableFrame().setVisible(true);
    }

}
