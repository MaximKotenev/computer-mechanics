package ru.maxim.preprocessor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import ru.maxim.construction.Node;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;

public class ForcesDialog extends javax.swing.JDialog 
{
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel		    jLabel1;
    private javax.swing.JLabel		    jLabel2;
    private javax.swing.JScrollPane	    jScrollPane1;
    private javax.swing.JScrollPane	    jScrollPane2;
    private javax.swing.JTable		    tableNodes;
    private javax.swing.JTable		    tableRods;
    // End of variables declaration//GEN-END:variables
    
    private List<Rod>			    rods = SchemeContainer.getRods();
    private List<Node>			    nodes = SchemeContainer.getNodes();
    private TableModelRods		    modelRods = new TableModelRods(rods);
    private TableModelNodes		    modelNodes = new TableModelNodes(nodes);

    
    
    public ForcesDialog(java.awt.Frame parent, boolean modal)
    {
        super(parent, modal);
        initComponents();
    }
    
    public class TableModelRods implements TableModel 
    {
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>(); 
        private List<Rod> beans;
 
        public TableModelRods(List<Rod> beans) {
            this.beans = beans;
        }
 
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
        public Class<?> getColumnClass(int columnIndex) {
	    switch (columnIndex) 
	    {
		case 0:
		    return Integer.class;
		case 1:
		    return Integer.class;
	    }
	    return Object.class;
        }
 
	@Override
        public int getColumnCount() {
            return 2;
        }
 
	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
            case 0:
                return "№";
            case 1:
                return "Нагрузка";
            }
            return "";
        }
 
        public int getRowCount() {
            return beans.size();
        }
 
        public Object getValueAt(int rowIndex, int columnIndex) {
            Rod bean = beans.get(rowIndex);
            switch (columnIndex) 
	    {
		case 0:
		    return rowIndex;
		case 1:
		    return bean.getStress();
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
	    if (columnIndex == 0)
		return false;
            return true;
        }
 
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
        public void setValueAt(Object value, int rowIndex, int columnIndex) 
	{
	   Rod bean = beans.get(rowIndex);
            switch (columnIndex)
	    {
		case 0:
		    break;
		case 1:
		    bean.setStress((int) value);
		    break;
            }
        }
    }
    
    public class TableModelNodes implements TableModel 
    {
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>(); 
        private List<Node> beans;
 
        public TableModelNodes(List<Node> beans) {
            this.beans = beans;
        }
 
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
        public Class<?> getColumnClass(int columnIndex) {
	    switch (columnIndex) 
	    {
		case 0:
		    return Integer.class;
		case 1:
		    return Integer.class;
		case 2:
		    return Boolean.class;
	    }
	    return Object.class;
        }
 
	@Override
        public int getColumnCount() {
            return 3;
        }
 
	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
            case 0:
                return "№";
            case 1:
                return "Нагрузка";
            case 2:
                return "Заделка";
            }
            return "";
        }
 
        public int getRowCount() {
            return beans.size();
        }
 
        public Object getValueAt(int rowIndex, int columnIndex) {
            Node bean = beans.get(rowIndex);
            switch (columnIndex) 
	    {
		case 0:
		    return rowIndex;
		case 1:
		    return bean.getStress();
		case 2:
		    return bean.isSealing();
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
	    if (columnIndex == 0)
		return false;
            return true;
        }
 
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
        public void setValueAt(Object value, int rowIndex, int columnIndex) 
	{
	   Node bean = beans.get(rowIndex);
            switch (columnIndex)
	    {
		case 0:
		    break;
		case 1:
		    bean.setStress((int) value);
		    break;
		case 2:
		    bean.setSealing((boolean) value);
		    break;
            }
        }
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableRods = new javax.swing.JTable(modelRods);
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableNodes = new javax.swing.JTable(modelNodes);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(612, 405));

        jScrollPane1.setViewportView(tableRods);
        jLabel1.setText("Стержни:");
        jLabel2.setText("Узлы:");
        jScrollPane2.setViewportView(tableNodes);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(265, 265, 265)
                                .addComponent(jLabel1))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(282, 282, 282)
                                .addComponent(jLabel2)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                            .addComponent(jScrollPane2))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public static void main(String args[]) 
    {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ForcesDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ForcesDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ForcesDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ForcesDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ForcesDialog dialog = new ForcesDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
	
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30

    }
}
