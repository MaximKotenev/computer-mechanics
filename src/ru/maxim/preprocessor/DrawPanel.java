package ru.maxim.preprocessor;

import java.util.List;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import ru.maxim.construction.Node;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;

public class DrawPanel extends JPanel
{
    public static int				    WIDTH = 640;
    public static int				    HEIGHT = 480;

    private BufferedImage			    image;
    private Graphics2D				    g;
    
    private double				    xCoof;
    private double				    yCoof;
    
    private final BasicStroke			    usualStroke;
    private final BasicStroke			    fatStroke;
    
    private List<Rod>                               rods;
    private List<Node>                              nodes;



    public				    DrawPanel()
    {
	super();
	setPreferredSize(new Dimension(WIDTH, HEIGHT));
	
	image = new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_RGB);
	g = (Graphics2D) image.getGraphics();
	
	usualStroke = new BasicStroke(1.0f);
	fatStroke = new BasicStroke(5.0f);
    }
    
    @Override
    public Dimension			    getPreferredSize()
    {
	return new Dimension(WIDTH, HEIGHT);
    }
    
    @Override
    public void				    paintComponent(Graphics g)
    {
	rods = SchemeContainer.getRods();
	nodes = SchemeContainer.getNodes();
	
	if (rods.isEmpty())
	    return;

	xCoof = WIDTH / SchemeContainer.getWholeLength();
        
        // <editor-fold defaultstate="collapsed" desc="Old shit">//GEN-BEGIN:initComponents
        {
//	if (SchemeContainer.getMaxHeight() < 1)
//	    yCoof = HEIGHT / 1;
//	else
//	    yCoof = HEIGHT / SchemeContainer.getMaxHeight();
        }
// </editor-fold>//GEN-END:Old shit

	yCoof = HEIGHT / SchemeContainer.getMaxSquare();
        if (yCoof <= 0)
            yCoof = SchemeContainer.getMaxSquare() / HEIGHT - 0.2;
		
	Graphics2D g2 = (Graphics2D) g;
	
	clearScreen(g2);
	drawRods(g2);
	drawNodes(g2);
    }
    
    private void			    clearScreen(Graphics2D g2)
    {
	g2.setColor(Color.WHITE);
	g2.fillRect(0, 0, WIDTH, HEIGHT);
    }
    
    private void			    drawRods(Graphics2D g2)
    {
	for (Rod rod : rods)
	{
	    int yOffset = (int) ((HEIGHT - (rod.getSquare() * yCoof)) / 2);
	 
	    g2.setStroke(fatStroke);
	    g2.setColor(Color.RED);
	    g2.drawRect((int) (rod.getX() * xCoof), yOffset,
		    (int) (rod.getLength() * xCoof), (int) (rod.getSquare() * yCoof));
	    g2.setColor(Color.BLACK);
	    g2.drawString("L(" + (int) rod.getLength() + ") S(" + rod.getSquare() + ")",
		    (int) (rod.getX() * xCoof), yOffset + 10);
	    
	    if (rod.getStress() != 0)
	    {
		g2.setStroke(usualStroke);
		g2.setColor(Color.BLUE);
		
		g2.drawString("" + rod.getStress(), (int) ((rod.getX() * xCoof + (rod.getLength() * xCoof) / 2) ), (HEIGHT / 2));
		g2.drawLine((int) (rod.getX() * xCoof), (HEIGHT / 2), (int) (rod.getX() * xCoof + rod.getLength() * xCoof), (HEIGHT / 2));
		for (int count = 0, rx = (int) (rod.getX() * xCoof); count < 4;
			count++, rx += (int) ((rod.getLength() * xCoof) / 3))
		{
		    if (rod.getStress() > 0)
			g2.drawString(">", rx, (HEIGHT / 2 + 5));
		    else
			g2.drawString("<", rx, (HEIGHT / 2 + 5));
		}
	    }
	}
    }
    
    private void			    drawNodes(Graphics2D g2)
    {
	for (int i = 0; i < nodes.size(); i++)
	{
            Node n = nodes.get(i);
            
	    g2.setStroke(fatStroke);
	    g2.setColor(Color.DARK_GRAY);
	    g2.drawOval((int) (n.getX() * xCoof), HEIGHT / 2, 5, 5);
	    
	    if (n.isSealing())
            {
                g2.drawLine((int) (n.getX() * xCoof), 0, (int) (n.getX() * xCoof), HEIGHT);
                for (int rx = 0, coof = 10; rx < HEIGHT; rx += 30)
                {
                    g2.drawLine((int) (n.getX() * xCoof), rx, 
                            (int) (n.getX() * xCoof - coof), rx - coof);
                    g2.drawLine((int) (n.getX() * xCoof), rx, 
                            (int) (n.getX() * xCoof + coof), rx - coof);
                }
//		g2.drawImage(sealing, (int) (n.getX() * xCoof), HEIGHT / 2 - sealing.getHeight(this) / 2, this);
            }
	    if (n.getStress() != 0)
	    {
		g2.setStroke(usualStroke);
		g2.setColor(Color.GREEN);
		if (n.getStress() > 0)
		{
                    double off = xCoof / 2;
                    if (i < rods.size())
                        off = rods.get(i).getLength() / 2 * xCoof;
                    
		    g2.drawLine((int) (n.getX() * xCoof), HEIGHT / 2,
                            (int) (n.getX() * xCoof + off), HEIGHT / 2);
		    g2.drawString(">",
                            (int) (n.getX() * xCoof + off - 2), HEIGHT / 2 + 5);
		    g2.drawString("" + n.getStress(),
                            (int) (n.getX() * xCoof + off / 2), HEIGHT / 2);
		}
		else
		{
                    double off = xCoof / 2;
                    if (i > 0)
                        off = rods.get(i - 1).getLength() / 2 * xCoof;
                    
		    g2.drawLine((int) (n.getX() * xCoof), HEIGHT / 2,
                            (int) (n.getX() * xCoof - off), HEIGHT / 2);
		    g2.drawString("<",
                            (int) (n.getX() * xCoof - off - 2), HEIGHT / 2 + 5);
                    g2.drawString("" + n.getStress(),
                            (int) (n.getX() * xCoof - off), HEIGHT / 2);
		}
	    }
	}
    }


    public static void main(String[] args)
    {
	DrawPanel p = new DrawPanel();
		
//	SchemeContainer.addRod(new Rod(1, 1, 1, 1)); // 10 X 15
//	SchemeContainer.addRod(new Rod(2, 2, 1, 1)); // 10 X 15
//	SchemeContainer.addRod(new Rod(2, 2, 1, 1)); // 10 X 15
//	SchemeContainer.addRod(new Rod(1, 1, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(20, 300, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(20, 300, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(100, 300, 1, 1)); // 20 X 30
	
	SchemeContainer.getRods().get(0).setStress(10);
	SchemeContainer.getRods().get(1).setStress(-10);
	SchemeContainer.getRods().get(2).setStress(10);
	SchemeContainer.getRods().get(3).setStress(-10);
	SchemeContainer.getRods().get(6).setStress(-10);
	
	SchemeContainer.getNodes().get(5).setStress(-10);
        
	SchemeContainer.getNodes().get(0).setSealing(true);
	SchemeContainer.getNodes().get(7).setSealing(true);
	
//	p.xCoof = WIDTH / SchemeContainer.getWholeLength();
//	int hh = SchemeContainer.getMaxHeight();
//	if (hh < 1)
//	    hh = 1;
//	p.yCoof = HEIGHT / hh;	
		
	JFrame f = new JFrame();
	f.setContentPane(p);
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	f.pack();
	f.setVisible(true);
    }
}