package ru.maxim.preprocessor;

import java.awt.event.ActionEvent;
import javax.swing.JDialog;

public class PreprocessorMainFrame extends javax.swing.JFrame 
{
    private javax.swing.JButton			    buttonAddRod;
    private javax.swing.JButton			    buttonChangeForces;
    private javax.swing.JButton			    buttonDraw;

    private RodsSettingsDialog			    rodsSettingsDialog;
    private JDialog				    drawDialog;
    private DrawPanel				    panel;
    private ForcesDialog			    changeForcesDialog;

    
    public				    PreprocessorMainFrame()
    {
        initComponents();
	this.setVisible(true);
				
	buttonAddRod.addActionListener((ActionEvent e) -> {
	    if (rodsSettingsDialog == null)
		rodsSettingsDialog = new RodsSettingsDialog(this, true);
	    rodsSettingsDialog.setVisible(true);
	});
	
	buttonChangeForces.addActionListener((ActionEvent e) -> {
	    if (changeForcesDialog == null)
		changeForcesDialog = new ForcesDialog(this, true);
	    changeForcesDialog.setVisible(true);
	});
	
	drawDialog = new JDialog();
	panel = new DrawPanel();
	drawDialog.setContentPane(panel);
	drawDialog.pack();
	
	buttonDraw.addActionListener((ActionEvent e) -> {
	    drawDialog.setVisible(true);
	});
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonAddRod = new javax.swing.JButton();
        buttonChangeForces = new javax.swing.JButton();
        buttonDraw = new javax.swing.JButton();

        buttonAddRod.setText("Изменить конструкцию");

        buttonChangeForces.setText("Изменить нагрузки");

        buttonDraw.setText("Отрисовать");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonChangeForces, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonAddRod, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                    .addComponent(buttonDraw, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(116, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonAddRod, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(buttonChangeForces, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(buttonDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

}
