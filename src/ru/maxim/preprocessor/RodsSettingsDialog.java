package ru.maxim.preprocessor;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;

public class RodsSettingsDialog extends javax.swing.JDialog
{
    private javax.swing.JButton		    buttonAdd;
    private javax.swing.JButton		    buttonDelete;
    private javax.swing.JTextField	    fieldNumberDelete;
    private javax.swing.JScrollPane	    jScrollPane1;
    private javax.swing.JTable		    table;
    
    private List<Rod>			    rods = SchemeContainer.getRods();
    private TableModel			    model = new MyTableModel(rods);
    
    private boolean			    isRecalculate = false;



    public RodsSettingsDialog(java.awt.Frame parent, boolean modal)
    {
        super(parent, modal);
        initComponents();
	
	buttonAdd.addActionListener((ActionEvent e) -> {
	    SchemeContainer.addRod(new Rod(10, 10, 1, 1));
	    isRecalculate = true;
	    table.updateUI();
	});
	
	buttonDelete.addActionListener((ActionEvent e) -> {
	    int num = Integer.parseInt(fieldNumberDelete.getText());
	    if ( num >= 0 && num < rods.size())
		SchemeContainer.removeRod(num);
	    isRecalculate = true;
	    table.updateUI();
	});
	
	this.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent e) {
		if (isRecalculate == true)
		    SchemeContainer.recalculate();
		isRecalculate = false;
	    }
	});
    }
    
    public class MyTableModel implements TableModel 
    {
        private Set<TableModelListener> listeners = new HashSet<TableModelListener>(); 
        private List<Rod> beans;
 
        public MyTableModel(List<Rod> beans) {
            this.beans = beans;
        }
 
        public void addTableModelListener(TableModelListener listener) {
            listeners.add(listener);
        }
 
        public Class<?> getColumnClass(int columnIndex) {
	    switch (columnIndex) 
	    {
		case 0:
		    return Integer.class;
		case 1:
		    return Double.class;
		case 2:
		    return Integer.class;
		case 3:
		    return Integer.class;
		case 4:
		    return Integer.class;
	    }
	    return Object.class;
        }
 
	@Override
        public int getColumnCount() {
            return 5;
        }
 
	@Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
            case 0:
                return "Номер";
            case 1:
                return "Длина";
            case 2:
                return "Площадь";
            case 3:
                return "Модуль упругости";
            case 4:
                return "Напряжение";
            }
            return "";
        }
 
        public int getRowCount() {
            return beans.size();
        }
 
        public Object getValueAt(int rowIndex, int columnIndex) {
            Rod bean = beans.get(rowIndex);
            switch (columnIndex) 
	    {
		case 0:
		    return rowIndex;
		case 1:
		    return bean.getLength();
		case 2:
		    return bean.getSquare();
		case 3:
		    return bean.getElasticModulus();
		case 4:
		    return bean.getLimitingStress();
            }
            return "";
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return true;
        }
 
        public void removeTableModelListener(TableModelListener listener) {
            listeners.remove(listener);
        }
 
        public void setValueAt(Object value, int rowIndex, int columnIndex) 
	{
	   Rod bean = beans.get(rowIndex);
            switch (columnIndex) 
	    {
		case 0:
		    return;
		case 1:
                    if ((double) value > 0)
                        bean.setLength((Double) value);
		    break;
		case 2:
                    if ((int) value > 0)
                        bean.setSquare((int) value);
		    break;
		case 3:
                    if ((int) value > 0)
                        bean.setElasticModulus((int) value);
		    break;
		case 4:
                    if ((int) value >= 0)
                        bean.setLimitingStress((int) value);
		    break;
            }
	    
	    isRecalculate = true;
        }
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(model);
        buttonAdd = new javax.swing.JButton();
        fieldNumberDelete = new javax.swing.JTextField();
        buttonDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jScrollPane1.setViewportView(table);

        buttonAdd.setText("Добавить стержень");

        buttonDelete.setText("Удалить стержень");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 747, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(buttonAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(fieldNumberDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(259, 259, 259))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fieldNumberDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(113, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) 
    {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RodsSettingsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RodsSettingsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RodsSettingsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RodsSettingsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                RodsSettingsDialog dialog = new RodsSettingsDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
	
	SchemeContainer.addRod(new Rod(15, 150, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 600, 1, 1)); // 20 X 30

    }
}
