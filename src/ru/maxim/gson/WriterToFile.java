package ru.maxim.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import ru.maxim.construction.Node;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.processor.MainCalculations;

public class WriterToFile
{
    private List<Node> nodes;
    private List<Rod> rods;
    private MainCalculations calc;
    
    private Gson gson;
    
    private File fileNodes;
    private File fileRods;
    private File fileCalculations;

    public WriterToFile() 
    {
        nodes = SchemeContainer.getNodes();
        rods = SchemeContainer.getRods();
        calc = MainCalculations.getInstance();
        fileNodes = new File("files/gson/nodes.txt");
        fileRods = new File("files/gson/rods.txt");
        fileCalculations = new File("files/gson/processor.txt");
        
        gson = new GsonBuilder().create();
        writeNodes();
        writeRods();
        writeCalculations();
    }
    
    private void writeNodes()
    {
        String toJson = gson.toJson(nodes);
        try(FileWriter writer = new FileWriter(fileNodes, false)) {
            writer.write(toJson);
        } catch(IOException e) { e.printStackTrace(); }
    }
    
    private void writeRods()
    {
        String toJson = gson.toJson(rods);
        try(FileWriter writer = new FileWriter(fileRods, false)) {
            writer.write(toJson);
        } catch(IOException e) { e.printStackTrace(); }
    }
    
    private void writeCalculations()
    {
        String toJson = gson.toJson(calc);
        try(FileWriter writer = new FileWriter(fileCalculations, false)) {
            writer.write(toJson);
        } catch(IOException e) { e.printStackTrace(); }
    }
}