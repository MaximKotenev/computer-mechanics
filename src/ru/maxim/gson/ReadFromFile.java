package ru.maxim.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;
import ru.maxim.construction.Node;
import ru.maxim.construction.Rod;
import ru.maxim.construction.SchemeContainer;

public class ReadFromFile 
{    
    private Gson gson;
    
    private File fileNodes;
    private File fileRods;

    public ReadFromFile()
    {
        fileNodes = new File("files/gson/nodes.txt");
        fileRods = new File("files/gson/rods.txt");

        gson = new GsonBuilder().create();
        readNodes();
        readRods();
    }
    
    private void readNodes()
    {
        try {
            FileReader reader = new FileReader(fileNodes);
            Type itemsListType = new TypeToken<List<Node>>() {}.getType();
            List<Node> getNodes = gson.fromJson(reader, itemsListType);
            SchemeContainer.setNodes(getNodes);

        } catch (FileNotFoundException e) { e.printStackTrace(); }
    }
    
    private void readRods()
    {
        try {
            FileReader reader = new FileReader(fileRods);
            
            Type itemsListType = new TypeToken<List<Rod>>() {}.getType();
            List<Rod> getRods = gson.fromJson(reader, itemsListType);
            SchemeContainer.setRods(getRods);

        } catch (FileNotFoundException e) { e.printStackTrace(); }
    }
}