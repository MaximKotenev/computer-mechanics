package ru.maxim.main;

import java.awt.Toolkit;
import ru.maxim.gson.WriterToFile;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.UIManager;
import ru.maxim.construction.SchemeContainer;
import ru.maxim.gson.ReadFromFile;
import ru.maxim.postprocessor.PostProcessorFrame;
import ru.maxim.preprocessor.PreprocessorMainFrame;
import ru.maxim.processor.ProcessorFrame;

public class MainFrame extends javax.swing.JFrame 
{
    private javax.swing.JLabel			jLabel1;
    private javax.swing.JButton			postprocessorButton;
    private javax.swing.JButton			preprocessorButton;
    private javax.swing.JButton			processorButton;
    
    private PreprocessorMainFrame		preprocessorFrame;
    private ProcessorFrame			processorFrame;
    private PostProcessorFrame			postprocessorFrame;
    private ErrorSingularMatrix			error = new ErrorSingularMatrix();



    public MainFrame() 
    {
        ReadFromFile r = new ReadFromFile();
        try { 
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch(Exception e){ e.printStackTrace(); }

        initComponents();

        
        setLocationRelativeTo(null);
	setVisible(true);
        
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                WriterToFile w = new WriterToFile();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        preprocessorButton = new javax.swing.JButton();
        processorButton = new javax.swing.JButton();
        postprocessorButton = new javax.swing.JButton();
	postprocessorButton.setEnabled(false);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("<html>\n<center>\n\t<h2>\n\tПрограмма САПР.\n\tАвтор: Котенев М.В.\n\t</h2>\n </center>");

        preprocessorButton.setText("Препроцессор");
        preprocessorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preprocessorButtonActionPerformed(evt);
            }
        });

        processorButton.setText("Процессор");
        processorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processorButtonActionPerformed(evt);
            }
        });

        postprocessorButton.setText("Постпроцессор");
        postprocessorButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                postprocessorButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(135, 135, 135)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(preprocessorButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(processorButton, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                    .addComponent(postprocessorButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE))
                .addContainerGap(130, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(preprocessorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(processorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(postprocessorButton, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(85, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void preprocessorButtonActionPerformed(java.awt.event.ActionEvent evt) 
    {
        if (preprocessorFrame == null)
	    preprocessorFrame = new PreprocessorMainFrame();
	preprocessorFrame.setVisible(true);
    }

    private void processorButtonActionPerformed(java.awt.event.ActionEvent evt) 
    {
	if (!SchemeContainer.checkSealingBySides())
	{
	    error.setVisible(true);
	    return;
	}
	
	if (processorFrame == null)
	{
	    processorFrame = new ProcessorFrame();
	    postprocessorButton.setEnabled(true);
	}
	
	processorFrame.solve();
	processorFrame.setVisible(true);
    }

    private void postprocessorButtonActionPerformed(java.awt.event.ActionEvent evt) 
    {
	if (postprocessorFrame == null)
	    postprocessorFrame = new PostProcessorFrame();
	postprocessorFrame.setVisible(true);
    }

    public static void main(String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
}
