package ru.maxim.processor;

public class ProcessorFrame extends javax.swing.JFrame 
{
    private javax.swing.JScrollPane		jScrollPane2;
    private javax.swing.JTextArea		textSolution;

    private MainCalculations			processorCalculation;
    
    
    
    public				ProcessorFrame() 
    {
	processorCalculation = MainCalculations.getInstance();
        initComponents();
    }
    
    public void				solve()
    {
	processorCalculation.recalculate();
	
	textSolution.setText(
		processorCalculation.getTextMatrixA() + 
		processorCalculation.getTextVectorB() + 
		processorCalculation.getTextVectorDeltaSolution() +
		processorCalculation.getTextU() + 
		processorCalculation.getTextNx() +
		processorCalculation.getTextSigma() + 
                processorCalculation.getTextUx());
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        textSolution = new javax.swing.JTextArea();
	textSolution.setEditable(false);

        textSolution.setColumns(20);
        textSolution.setRows(5);
        jScrollPane2.setViewportView(textSolution);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
}
