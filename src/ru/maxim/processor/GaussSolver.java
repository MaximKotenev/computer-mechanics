package ru.maxim.processor;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularMatrixException;


public class GaussSolver
{
    private double[][]				    matrix;
    private double[]				    vector;
    
    
    
    public					GaussSolver(double[][] matrix, double[] vector)
    {
	this.matrix = matrix;
	this.vector = vector;
    }
    
    public double[]				solveSystem()
    {
	RealMatrix coef = new Array2DRowRealMatrix(matrix);
	DecompositionSolver solver = new LUDecomposition(coef).getSolver();
	RealVector right = new ArrayRealVector(vector);
	RealVector x = null;
	try {
	    x = solver.solve(right);
	} catch (SingularMatrixException e) { 
	    e.printStackTrace();
	}
	double[] ans = x.toArray();
	
	System.out.println("Delta:");
	for (double d:ans) {
	    System.out.println(d);
	}

	return ans;
    }
    
    public double[]				solveSystem(double[][] matrix, double[] vector)
    {
	this.matrix = matrix;
	this.vector = vector;
	return solveSystem();
    }
}
