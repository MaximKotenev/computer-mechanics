package ru.maxim.processor;

import java.util.Arrays;
import ru.maxim.construction.*;

public class MainCalculations
{
    private static MainCalculations		    instance = new MainCalculations();
    
    private Rod[]				    rods;
    private Node[]				    nodes;
    
    private double[][]				    matrixA;
    private double[]				    vectorB;
    
    private GaussSolver				    solver;
    private double[]				    vectorSolution;
    
    private double[]				    U;
    private double[]				    Nx;
    private double[]				    sigma;


    
    public static MainCalculations		    getInstance()
    {
	return instance;
    }
    
    private					    MainCalculations()
    {
    }

    public void				    recalculate()
    {
	init();
    }
    
    private void			    init()
    {
	rods = new Rod[SchemeContainer.getRods().size()];
	nodes = new Node[SchemeContainer.getNodes().size()];
	
	for (int i = 0; i < rods.length; i++)
	    rods[i] = SchemeContainer.getRods().get(i);
	for (int i = 0; i < nodes.length; i++)
	    nodes[i] = SchemeContainer.getNodes().get(i);
	
	matrixA = new double[nodes.length][nodes.length];
	initMatrixA();
	printMatrixA();
	
	vectorB = new double[nodes.length];
	initVectorB();

	solver = new GaussSolver(matrixA, vectorB);
	vectorSolution = solver.solveSystem();
	
	U = new double[rods.length * 2];
	initU();
	System.out.print("\nU:\n" + Arrays.toString(U) + "\n");
	
	initNx();
	
	initSigma();
    }
    
    private void			    initMatrixA()
    {
	matrixA[0][0] = rods[0].getElasticModulus() * rods[0].getSquare() / rods[0].getLength();
	matrixA[0][1] = matrixA[1][0] = -matrixA[0][0];

	for (int i = 1; i < rods.length; i++)
	{
	    matrixA[i][i] = rods[i].getElasticModulus() * rods[i].getSquare() / rods[i].getLength();
	    matrixA[i][i+1] = matrixA[i+1][i] = -matrixA[i][i];
	    matrixA[i][i] += rods[i-1].getElasticModulus() * rods[i-1].getSquare() / rods[i-1].getLength();;
	}
	matrixA[matrixA.length-1][matrixA.length-1] = 
		rods[rods.length-1].getElasticModulus() * rods[rods.length-1].getSquare() / rods[rods.length-1].getLength();
		
	// ставим заделки
	for (int i = 0; i < nodes.length; i++)
	{
	    if (nodes[i].isSealing() && i != nodes.length - 1) {
		matrixA[i][i] = 1;
		matrixA[i+1][i] = matrixA[i][i+1] = 0;
	    }
	    if (nodes[i].isSealing() && i == nodes.length - 1) {
		matrixA[i][i] = 1;
		matrixA[i-1][i] = matrixA[i][i-1] = 0;
	    }
	}
    }
    
    private void			    initVectorB()
    {
	vectorB[0] = (rods[0].getStress() * rods[0].getLength() / 2) + nodes[0].getStress();
	vectorB[vectorB.length-1] =
		(rods[vectorB.length-2].getStress() * rods[vectorB.length-2].getLength() / 2) + nodes[vectorB.length-1].getStress();
	
	for (int i = 1; i < vectorB.length - 1; i++)
	{
	    vectorB[i] = (rods[i-1].getStress() * rods[i-1].getLength() / 2) +
		    (rods[i].getStress() * rods[i].getLength() / 2) +
		    nodes[i].getStress();
	}
	
	for (int i = 0; i < nodes.length; i++)
	    if (nodes[i].isSealing())
		vectorB[i] = 0;
	
	printVectorB();
    }
    
    private void			    initU()
    {
	U[0] = vectorSolution[0];
	U[U.length-1] = vectorSolution[vectorSolution.length-1];
	for (int i = 1, j = 1; i < vectorSolution.length-1; i++, j += 2)
	    U[j] = U[j+1] = vectorSolution[i];
    }
    
    private void			    initNx()
    {
	int size = 0;
	for (Rod rod : rods) 
	    if (rod.getStress() != 0) size += 2;
	    else size++;
	Nx = new double[size];
	
	for (int i = 0, j = 0, u = 0; i < rods.length; i++, u += 2)
	{
	    double nx = rods[i].getElasticModulus() * rods[i].getSquare() / rods[i].getLength();
	    double diffU = U[u+1] - U[u];
	    nx *= diffU;
	    
	    if (rods[i].getStress() != 0)
	    {
		double part = rods[i].getStress() * rods[i].getLength() / 2;
		double part0 = 1 - 2 * 0 / rods[i].getLength();
		double partL = 1 - 2 * rods[i].getLength() / rods[i].getLength();
		Nx[j] = nx + (part * part0);
		Nx[j+1] = nx + (part * partL);
		
		j += 2;
	    }
	    else
	    {
		Nx[j] = nx;
		j++;
	    }
	}
	
	System.out.println("\nNx:");
	System.out.println(Arrays.toString(Nx));
    }
    
    private void			    initSigma()
    {
	sigma = new double[Nx.length];
	for (int i = 0, j = 0; i < rods.length; i++)
	{
	    if (rods[i].getStress() != 0)
	    {
		sigma[j] = Nx[j] / rods[i].getSquare();
		sigma[j+1] = Nx[j+1] / rods[i].getSquare();
		j += 2;
	    }
	    else
	    {
		sigma[j] = Nx[j] / rods[i].getSquare();
		j++;
	    }
	}
	
	System.out.println("\nSigma:\n" + Arrays.toString(sigma));
    }
    
    public double	    calculateNx(int i, double x)
    {	
	double nx = rods[i].getElasticModulus() * rods[i].getSquare() / rods[i].getLength();
	double diffU = U[i*2+1] - U[i*2];
	nx *= diffU;	
	double addPart = rods[i].getStress() * rods[i].getLength() / 2 *
		(1 - 2 * x / rods[i].getLength());
	nx += addPart;
	return nx;
    }
    
    public double	    calculateUx(int i, double x)
    {
	double startU = U[i*2];
	double sPart = (x / rods[i].getLength()) * (U[i*2+1] - U[i*2]);
	double tPart = (rods[i].getStress() * rods[i].getLength() * rods[i].getLength() * x) /
		(2 * rods[i].getElasticModulus() * rods[i].getSquare() * rods[i].getLength()) *
		(1 - (x / rods[i].getLength()));
	
	return startU + sPart + tPart;
    }
    
    private void			    printMatrixA()
    {
	System.out.println("[A]:");
	for (int i = 0; i < matrixA.length; i++)
	    System.out.println(Arrays.toString(matrixA[i]));
	System.out.println("");
    }
    
    private void			    printVectorB()
    {
	System.out.println("{b}:");
	System.out.println(Arrays.toString(vectorB));
	System.out.println("");
    }
    
    public String			    getTextMatrixA()
    {
	StringBuilder str = new StringBuilder();
	str.append("[A]:\n");
	
	for (double[] mA : matrixA) {
	    for (double a: mA)
		str.append(String.format("%.2f", a)).append("\t");
	    str.append("\n");
	}
	str.append("\n");

	return str.toString();
    }
    
    public String			    getTextVectorB()
    {
	return "{b}:\n" + Arrays.toString(vectorB) + "\n\n";
    }
    
    public String			    getTextVectorDeltaSolution()
    {
	StringBuilder str = new StringBuilder();
	str.append("Delta:\n");
	
	for (int i = 0; i < vectorSolution.length; i++)
	    str.append(String.format("%.2f", vectorSolution[i])).append("\n");
	str.append("\n");
	return str.toString();
    }
    
    public String			    getTextU()
    {
	StringBuilder str = new StringBuilder();
	str.append("U:\n");
	
	for (int i = 0; i < U.length; i++)
	    str.append(String.format("%.2f", U[i])).append(" ");
	str.append("\n\n");
	return str.toString();
    }
    
    public String			    getTextNx()
    {
	StringBuilder str = new StringBuilder();
	str.append("Nx:\n");
	
	for (int i = 0; i < Nx.length; i++)
	    str.append(String.format("%.2f", Nx[i])).append(" ");
	str.append("\n\n");
	return str.toString();
    }
    
    public String			    getTextSigma()
    {
	StringBuilder str = new StringBuilder();
	str.append("Sigma:\n");
	
	for (int i = 0; i < sigma.length; i++)
	    str.append(String.format("%.2f", sigma[i])).append(" ");
	str.append("\n\n");
	return str.toString();
    }
    
    public String                           getTextUx()
    {
        StringBuilder str = new StringBuilder();
	str.append("U(x):\n");
	
        for (int i = 0; i < rods.length; i++)
            str.append(
                    String.format("%.2f", calculateUx(i, 0))).append(" ").
                    append(String.format("%.2f", calculateUx(i, rods[i].getLength()))).
                    append("\n");
        str.append("\n");
	return str.toString();
    }
    
    public double[]			    getNx()
    {
	return Nx;
    }
    
    public double[]			    getSigma()
    {
	return sigma;
    }
    
//    public static void			    setNx(double[] nx)
//    {
//	Nx = nx;
//    }
    public void				    setSigma(double[] sigma)
    {
	this.sigma = sigma;
    }
    

    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static void			    test()
    {
	SchemeContainer.addRod(new Rod(10, 10, 1, 1)); // 10 X 15
	SchemeContainer.addRod(new Rod(20, 20, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(20, 20, 1, 1)); // 20 X 30
	SchemeContainer.addRod(new Rod(15, 10, 1, 1)); // 10 X 15
	
	SchemeContainer.getRods().get(0).setStress(3);
	SchemeContainer.getRods().get(1).setStress(-10);
	SchemeContainer.getRods().get(2).setStress(4);
	SchemeContainer.getRods().get(3).setStress(-6);
	
	SchemeContainer.getNodes().get(1).setStress(9);

	SchemeContainer.getNodes().get(0).setSealing(true);
	SchemeContainer.getNodes().get(4).setSealing(true);
    }
    
    public static void main(String[] args) {
	test();
	MainCalculations c = new MainCalculations();
	c.recalculate();
	
//	for (double x = 0; x < c.rods[0].getLength(); x += c.rods[0].getLength() / 10)
//	{
//	    System.out.println(c.calculateNx(0, x));
//	}
    }
}
