//package ru.maxim.old;
//
//import com.jogamp.opengl.GL;
//import com.jogamp.opengl.GL2;
//import com.jogamp.opengl.GLAutoDrawable;
//import com.jogamp.opengl.GLEventListener;
//import com.jogamp.opengl.awt.GLCanvas;
//import com.jogamp.opengl.glu.GLU;
//import com.jogamp.opengl.util.Animator;
//import com.jogamp.opengl.util.gl2.GLUT;
//import java.awt.BorderLayout;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.util.List;
//import javax.swing.JFrame;
//import ru.maxim.construction.Rod;
//import ru.maxim.construction.SchemeContainer;
//
//public class DrawingFrame extends JFrame
//{
//    public GLCanvas			    canvas;
//    private Animator			    animator;
//    public Renderer			    renderer;
//    
//    
//    
//    public			    DrawingFrame()
//    {
//	canvas = new GLCanvas();
//	renderer = new Renderer();
//	canvas.addGLEventListener(renderer);
//	canvas.setIgnoreRepaint(true);
//	canvas.setSize(640, 480);
//	
//	animator = new Animator(canvas);
//	animator.setRunAsFastAsPossible(true);
//	animator.start();
//	
//	setTitle("Sample OpenGL Java app");
//	setResizable(false);
//	setLayout(new BorderLayout());
//	add(canvas, BorderLayout.CENTER);
//	setSize(getPreferredSize());
//
//	addWindowListener(new WindowAdapter() {public void windowClosing(WindowEvent e) {/*animator.stop();*/dispose();}});
//    }
//    
//    public void			    recalculateResolution() {
//	renderer.recalculateResolution();
////	canvas.reshape(0, 0, 640, 480);
//    }
//}
//
//
//
//class Renderer implements GLEventListener
//{
//    double				    relativeWidth;
//    double				    relativeHeight;
////    int					    off;
//
//    
//    
//    public void			    recalculateResolution()
//    {	
//	List<Rod> rods = SchemeContainer.getRods();
//	if (rods.isEmpty())
//	    return;
//	relativeWidth = 0;
//	relativeHeight = rods.get(0).getHeight();
//	
//	for (int i = 0; i < rods.size(); i++)
//	{
//	    relativeWidth += rods.get(i).getLength();
//	    relativeHeight = rods.get(i).getHeight() > relativeHeight ? rods.get(i).getHeight() : relativeHeight;
//	}
//	
////	relativeWidth += off;
////	relativeHeight += off;
////	off = (int) (relativeWidth / 10);
//	System.out.println("recalculateResolution(): " + relativeWidth + " " + relativeHeight);
//    }
//    
//    
//    @Override
//    public void init(GLAutoDrawable glad) {
//    }
//
//    @Override
//    public void dispose(GLAutoDrawable glad) {	
//    }
//
//    @Override
//    public void display(GLAutoDrawable glad) 
//    {
////	GL2 gl2 = glad.getGL().getGL2();
////        GLUT glut = new GLUT();
////	List<Rod> rods = SchemeContainer.getRods();
////	
////        gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
////
////        gl2.glLoadIdentity();
////	gl2.glColor3f(1, 0, 0);
////        gl2.glBegin(GL.GL_LINE_STRIP);
////	System.out.println("display()" + relativeWidth + " " + relativeHeight);
////	    for (int i = 0; i < rods.size(); i++)
////	    {
////		Rod rod = rods.get(i);
////		int off = (int) ((relativeHeight - rod.getHeight()) / 2);
////		
////		System.out.println("x: " + rod.getX() + " " + "; length: " + rod.getLength() + "; height: " + rod.getHeight());
////		gl2.glVertex2f(rod.getX(), off);
////		gl2.glVertex2f(rod.getX(), (float) rod.getHeight() + off);
////		gl2.glVertex2f(rod.getX() + rod.getLength(), (float) rod.getHeight() + off);
////		gl2.glVertex2f(rod.getX() + rod.getLength(), off);
////		gl2.glVertex2f(rod.getX(), off);
////	    }
////        gl2.glEnd();
//
//    }
//
//    @Override
//    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) 
//    {
//	System.out.println("reshape()");
//	
//	GL2 gl2 = glad.getGL().getGL2();
//	GLU glu = new GLU();
//
//        gl2.glMatrixMode( GL2.GL_PROJECTION );
//        gl2.glLoadIdentity();
//        glu.gluOrtho2D(0.0f, relativeWidth, relativeHeight, 0.0f);
//        gl2.glMatrixMode( GL2.GL_MODELVIEW );
//        gl2.glLoadIdentity();
////        gl2.glViewport( 0, 0, width, height);
//    }
//}